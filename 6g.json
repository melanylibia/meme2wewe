{
    "NOTE": "When answering to this assessment, consider questions from the perspective of ANY ACTIVITY DONE ALONE OR TOGETHER at home, travel, vacation, business, job, volunteering, education, arts, sports, politics, hobbies, games, indoors, outdoors, ...",
    "KEY": {
        "W": "Wonder",
        "I": "Invention",
        "D": "Discernment",
        "G": "Galvanizing",
        "E": "Enablement",
        "T": "Tenacity"
    },
    "VALUE": {
        "Genius": 2,
        "Competence": 2,
        "Frustration": 2
    },
    "RESULT": {
        "Confidence Interval, %": 80
    },
    "RATING": {
        "Question": "On a scale of 1 to 5, with 1 being least and 5 being most, how would you describe your agreement with:",
        "Answer": {
            "1": "Strongly Disagree",
            "2": "Disagree",
            "3": "Undecided",
            "4": "Agree",
            "5": "Strongly Agree"
        }
    },
    "QUESTION": {
        "1-DAY ACTIVITIES:": {
            "I get JOY, PEACE and ENERGY when I spend 8 hours mostly at ...": {
                "thinking about why things are as they are and if anything could be improved.": {
                    "W": 1
                },
                "designing new solutions for recognized needs.": {
                    "I": 1
                },
                "assessing strengths of proposed ideas.": {
                    "D": 1
                },
                "producing excitement and activity around good ideas.": {
                    "G": 1
                },
                "supporting others on their terms and assisting in implementation of solutions.": {
                    "E": 1
                },
                "pushing implemention of solutions to completion for desired results.": {
                    "T": 1
                }
            }
        },
        "1-WEEK ACTIVITIES:": {
            "I get JOY, PEACE and ENERGY when my week days mostly consist of ...": {
                "seeing details like possible new opportunities around me.": {
                    "W": 1
                },
                "developing new creative ideas around recognized needs.": {
                    "I": 1
                },
                "sharing instinctive feedback and insights about suggested solutions.": {
                    "D": 1
                },
                "mobilising enthusiastic action around excellent solutions.": {
                    "G": 1
                },
                "offering my cooperation and help while taking into account needs of others.": {
                    "E": 1
                },
                "getting things done in highly productive manner.": {
                    "T": 1
                }
            }
        },
        "NATURAL TALENTS:": {
            "I get JOY, PEACE and ENERGY from ...": {
                "observing my surroundings, questioning current state of things, and spotting what should change.": {
                    "W": 1
                },
                "innovating, solving problems, and creating something from nothing.": {
                    "I": 1
                },
                "providing insightful guidance and feedback using instinctive judgement, common sense, and understanding of human needs.": {
                    "D": 1
                },
                "inspiring myself and others to act on decisions, thereby creating and maintaining energized momentum in everybody involved.": {
                    "G": 1
                },
                "responding rapidly to needs of others, assisting without imposing my own terms, and cooperating with various projects.": {
                    "E": 1
                },
                "striving for excellence, ensuring tasks are finalized, and seeing wanted impact.": {
                    "T": 1
                }
            }
        },
        "GUILT & SHAME:": {
            "Looking back on my life, I have felt guilt, regret, blame or shame (and even been bullied) about ...": {
                "being too shallow, lacking deep thoughts or curiosity.": {
                    "W": -1
                },
                "being too boring, lacking imagination or originality.": {
                    "I": -1
                },
                "being too dumb, lacking words or understanding of big picture.": {
                    "D": -1
                },
                "being too weak, lacking in leading people or charisma.": {
                    "G": -1
                },
                "being too uncaring, lacking in showing kindness or sympathy.": {
                    "E": -1
                },
                "being too unreliable, lacking in consistency or persistence.": {
                    "T": -1
                }
            }
        },
        "2 PACES OF ACTIVITY:": {
            "I get JOY, PEACE and ENERGY from ...": {
                "slower and steadier activities or for opportunities to slow things down.": {
                    "W": 1,
                    "D": 1,
                    "E": 1
                },
                "responding by action when surroundings or others provoke me to act, for example, by asking.": {
                    "W": 1,
                    "D": 1,
                    "E": 1
                },
                "faster and burstier activities or for opportunities to speed things up.": {
                    "I": 1,
                    "G": 1,
                    "T": 1
                },
                "initiating action or provoking others to act, for example, by asking.": {
                    "I": 1,
                    "G": 1,
                    "T": 1
                }
            }
        },
        "3 PHASES OF ACTIVITY: IDEATION, ACTIVATION, IMPLEMENTATION": {
            "I get JOY, PEACE and ENERGY from ...": {
                "ideation phases of activity, where potential needs for improvements are identified, and new solutions are developed.": {
                    "W": 1,
                    "I": 1,
                    "E": -1,
                    "T": -1
                },
                "activation phases of activity, where decisions are made about evaluated solutions, and enthusiastic movement is produced.": {
                    "D": 1,
                    "G": 1
                },
                "implementation phases of activity, where plans are executed, tasks are completed, and desired results achieved.": {
                    "E": 1,
                    "T": 1,
                    "W": -1,
                    "I": -1
                }
            }
        },
        "4 MEETINGS OF ACTIVITY: BRAINSTORMING, DECISION-MAKING, LAUNCH, EXECUTING:": {
            "I get JOY, PEACE and ENERGY from ...": {
                "brainstorming meetings, where various new feasible solutions are explored considering unmet potential and big picture, but this is not context for immediate results.": {
                    "W": 0.5,
                    "I": 1,
                    "D": 0.5,
                    "T": -1
                },
                "decision-making meetings, where significant problems or opportunities are addressed by finding better solutions, challenging existing solutions rather than accepting them.": {
                    "I": 0.5,
                    "D": 1,
                    "G": 0.5,
                    "T": -0.5
                },
                "launch meetings, where weekly status of planned ideas is reviewed, and tweaks to their implementation are agreed upon to drive progress, accepting existing solutions rather than challenging them.": {
                    "D": 0.5,
                    "G": 1,
                    "E": 0.5,
                    "W": -0.5
                },
                "executing meetings, where daily tasks, short-term tactics, and immediate support needs are discussed, but this is not context for questioning.": {
                    "G": 0.5,
                    "E": 1,
                    "T": 0.5,
                    "W": -1
                }
            }
        },
        "WE ALL DESIRE TO BE SEEN, UNDERSTOOD, AND WANTED:": {
            "I specifically desire for ...": {
                "thoughtfulness; any consideration or attention towards what I think, say or do.": {
                    "W": 1
                },
                "freedom; any liberty or empowerment towards what I think, say or do.": {
                    "I": 1
                },
                "trust; any belief or confidence towards what I think, say or do.": {
                    "D": 1
                },
                "response; any activity or reaction towards what I think, say or do.": {
                    "G": 1
                },
                "admiration; any appreciation or gratitude towards what I think, say or do.": {
                    "E": 1
                },
                "clarity; any consistency or simplicity towards what I think, say or do.": {
                    "T": 1
                }
            }
        },
        "WE ALL CAN FEEL FRUSTRATED WHEN NOT SEEN, NOT UNDERSTOOD, OR NOT WANTED:": {
            "I specifically am frustrated when I feel ...": {
                "uninterest; any inconsideration or lack of curiosity.": {
                    "W": 1
                },
                "constrained; any forcing or being overly controlled.": {
                    "I": 1
                },
                "doubted; any scepticism or mistrust.": {
                    "D": 1
                },
                "apathy; any passivity or lack of enthusiasm.": {
                    "G": 1
                },
                "overlooked; any ingratitude or unappreciation.": {
                    "E": 1
                },
                "vagueness; any uncertainty or unnecessary complexity.": {
                    "T": 1
                }
            }
        }
    }
}